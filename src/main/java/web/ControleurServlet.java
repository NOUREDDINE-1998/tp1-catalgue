package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Response;

import dao.IProduitDAO;
import dao.ProduitDAOImpl;
import metier.entitie.Produit;

public class ControleurServlet extends HttpServlet{
private IProduitDAO metier;

@Override
public void init() throws ServletException {
	metier= new ProduitDAOImpl();
}

@Override
protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	String path=request.getServletPath();
	if(path.equals("/index.do")) {
		request.getRequestDispatcher("produit.jsp").forward(request, response);
	}
	else if(path.equals("/chercher.do")) {
		String motCle=request.getParameter("motCle");
		ProduitModel model= new ProduitModel();
		model.setMotCle(motCle);
		List<Produit> produits=metier.ProduitsParMC("%"+motCle+"%");
		model.setProduits(produits);
		request.setAttribute("model", model);
		request.getRequestDispatcher("produit.jsp").forward(request, response);
	}
	else if(path.equals("/saisie.do")) {
		request.setAttribute("produit", new Produit());
		request.getRequestDispatcher("saisieProduit.jsp").forward(request, response);
	}
	else if(path.equals("/saveProduit.do") &&(request.getMethod().equals("POST"))) {
		String des=request.getParameter("designation");
		double prix=Double.parseDouble(request.getParameter("prix"));
		int quantite=Integer.parseInt(request.getParameter("quantite"));
		Produit p= metier.save(new Produit(des,prix,quantite));
		request.setAttribute("produit", p);
		request.getRequestDispatcher("confirmation.jsp").forward(request, response);
	}
	else if(path.equals("/supprimer.do")){
		Long id=Long.parseLong(request.getParameter("id"));
		metier.deleteProduit(id);
		//request.getRequestDispatcher("produit.jsp").forward(request, response);
		response.sendRedirect("chercher.do?motCle=");
	}
	else if(path.equals("/edit.do")){ 
		Long id=Long.parseLong(request.getParameter("id"));
		Produit p=metier.getProduit(id);
		request.setAttribute("produit", p);
		request.getRequestDispatcher("editProduit.jsp").forward(request, response);
	}
	else if(path.equals("/updateProduit.do") &&(request.getMethod().equals("POST"))) {
		long id= Long.parseLong(request.getParameter("id"));
		String des=request.getParameter("designation");
		double prix=Double.parseDouble(request.getParameter("prix"));
		int quantite=Integer.parseInt(request.getParameter("quantite"));
		Produit p= new Produit(des,prix,quantite);
		p.setId(id);
		 metier.update(p);
		request.setAttribute("produit", p);
		request.getRequestDispatcher("confirmation.jsp").forward(request, response);
		
	}	
	else {	
		response.sendError(Response.SC_NOT_FOUND);
	}
	
}

@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
}








}
