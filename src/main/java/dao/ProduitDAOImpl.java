package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import metier.entitie.Produit;

public class ProduitDAOImpl implements IProduitDAO{

	@Override
	public Produit save(Produit p) {
		Connection connection=SingletonConnection.getConnection();
		try {
			String query="insert into produits(designation,prix,quantite) "
					+ "values (?,?,?)";
			PreparedStatement ps= connection.prepareStatement(query);
			ps.setString(1, p.getDesignation());
			ps.setDouble(2, p.getPrix());
			ps.setInt(3,p.getQuantite());
			ps.executeUpdate();
			PreparedStatement ps2= connection.prepareStatement("select MAX(id) as MAX_ID from produits");
			ResultSet rs= ps2.executeQuery();
			if(rs.next()) {
				p.setId(rs.getLong("MAX_ID"));
			}
		
			ps.close();
			}catch (SQLException e) {
			e.printStackTrace();
			}
			return p;
	}

	@Override
	public List<Produit> ProduitsParMC(String mc) {
		Connection connection=SingletonConnection.getConnection();
		List<Produit>produits= new ArrayList<Produit>();
		try {
			String sql="SELECT * FROM produits where designation like ? ";
			PreparedStatement ps= connection.prepareStatement(sql);
			ps.setString(1, mc);
			ResultSet rs= ps.executeQuery();
			while (rs.next()) {
				Produit produit = new Produit();
				produit.setId(rs.getLong("id"));
				produit.setDesignation(rs.getString("designation"));
				produit.setPrix(rs.getDouble("prix"));
				produit.setQuantite(rs.getInt("quantite"));
				produits.add(produit);
				
			}
			ps.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return produits;
	}

	@Override
	public Produit getProduit(Long id) {
		Connection connection=SingletonConnection.getConnection();
		Produit p=null;
		try {
			String sql="SELECT * FROM produits where id=? ";
			PreparedStatement ps= connection.prepareStatement(sql);
			ps.setLong(1, id);
			ResultSet rs= ps.executeQuery();
			if (rs.next()) {
				p = new Produit();
				p.setId(rs.getLong("id"));
				p.setDesignation(rs.getString("designation"));
				p.setPrix(rs.getDouble("prix"));
				p.setQuantite(rs.getInt("quantite"));
			}
			ps.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	@Override
	public Produit update(Produit p) {
		Connection connection=SingletonConnection.getConnection();
		try {
			String query="update  produits set designation=?,prix=?,quantite=? "
					+ "where id =?";
			PreparedStatement ps= connection.prepareStatement(query);
			ps.setString(1, p.getDesignation());
			ps.setDouble(2, p.getPrix());
			ps.setInt(3,p.getQuantite());
			ps.setLong(4,p.getId());
			ps.executeUpdate();
			ps.close();
			}catch (SQLException e) {
			e.printStackTrace();
			}
			return p;
	}

	@Override
	public void deleteProduit(Long id) {
		Connection connection=SingletonConnection.getConnection();
		try {
			String sql="delete FROM produits where id=? ";
			PreparedStatement ps= connection.prepareStatement(sql);
			ps.setLong(1, id);
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

}
