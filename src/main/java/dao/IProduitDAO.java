package dao;

import java.util.List;

import metier.entitie.Produit;

public interface IProduitDAO {
public Produit save(Produit p);
public List<Produit>ProduitsParMC(String mc);
public Produit getProduit(Long id);
public Produit update(Produit p);
public void deleteProduit(Long id);

}
