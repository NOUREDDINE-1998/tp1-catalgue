<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>Produits</title>
</head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<body>
<%@include file="header.jsp"%>
<div class="container col-md-10">
<div class="card ">
<div class="card-header bg-primary text-white"> Recherche des produits</div>
<div class="card-body">
<form action="chercher.do" method="get">
<label>Mot Cl�:</label>
<input type="text"name="motCle" value="${model.motCle}">
<button class="btn btn-primary"  type="submit">Calculer</button> 
</form>
<table class="table table-striped">
<tr>
<th>Id</th><th>Designation</th><th>Prix</th><th>Quantite</th>
</tr>
<c:forEach items="${model.produits}" var="p">
<tr>
<td>${p.id}</td>
<td>${p.designation}</td>
<td>${p.prix}</td>
<td>${p.quantite}</td>
<td><a href="supprimer.do?id=${p.id}" onclick="return confirm('Etes vous sure?')">Supprime</a></td>
<td><a href="edit.do?id=${p.id}">Edit</a></td>
</tr>
</c:forEach>
</table>
</div>
</div>
</div>





</body>
</html>