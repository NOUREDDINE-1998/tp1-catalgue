<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>Produits</title>
</head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<body>
<%@include file="header.jsp"%>
<div class="container col-md-10">
<div class="card ">
<div class="card-header bg-primary text-white">Confirmation</div>
<div class="card-body">    
<div class="form-group">
      <label  >Id:</label>
     <label  >${produit.id}</label>
    </div>
    <div class="form-group">
      <label  >Designation:</label>
     <label  >${produit.designation}</label>
    </div>
    <div class="form-group">
      <label  >Prix:</label>
     <label  >${produit.prix}</label>
    </div>
    <div class="form-group">
      <label>Quantite:</label>
     <label>${produit.quantite}</label>
    </div>
</div>
</div>
</div>





</body>
</html>