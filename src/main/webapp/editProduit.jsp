<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>Produits</title>
</head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<body>
<%@include file="header.jsp"%>
<div class="container col-md-10">
<div class="card ">
<div class="card-header bg-primary text-white"> Saisie Produit</div>
<div class="card-body">
<form action="updateProduit.do" method="post">
 <div class="form-group">
      <label class="control-label " >Id:</label>
      <div >
        <input type="text" class="form-control" name="id" readonly="readonly" 
        value="${produit.id}">
      </div>
    </div>
  <div class="form-group">
      <label class="control-label " >Designation:</label>
      <div >
        <input type="text" class="form-control" name="designation"  value="${produit.designation}">
      </div>
    </div>
    
    <div class="form-group">
      <label class="control-label " >Prix:</label>
      <div >
        <input type="text" class="form-control" name="prix" value="${produit.prix}" >
      </div>
    </div>
    
    <div class="form-group">
      <label class="control-label " >Quantite:</label>
      <div >
        <input type="text" class="form-control" name="quantite" value="${produit.quantite}"  >
      </div>
    </div>
    <div>
    <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>
</div>
</div>
</div>

</body>
</html>